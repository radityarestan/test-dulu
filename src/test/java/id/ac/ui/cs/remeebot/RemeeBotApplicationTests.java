package id.ac.ui.cs.remeebot;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RemeeBotApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    public void testApplication() {
        RemeeBotApplication.main(new String[] {});
    }

}
