package id.ac.ui.cs.remeebot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RemeeBotController {
    @RequestMapping(method = RequestMethod.GET)
    private String home() {
        return "home";
    }
}
